package com.poc.s3.insights.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.poc.s3.insights.encryption.FileEncryptionManager;

public class UploadDataS3 {
	
	public  static String uploadFile(AWSCredentials awsCredentials,String bucketName,InputStream inFile) {
		try {
			AmazonS3 s3client = getS3Client(awsCredentials);
			uploadObject(s3client, bucketName,inFile);
		}catch(Exception e) {
			return e.getMessage();
		}
		return "UPLOAD SUCCESS";
	}
	
	public  static File downloadFile(AWSCredentials awsCredentials,String bucketName) {
		File file = null;
		try {
			AmazonS3 s3client = getS3Client(awsCredentials);
			file = downloadObject(s3client, bucketName);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return file;
	}

	private static void uploadObject(AmazonS3 s3client,String bucketName,InputStream inFile) {
		System.out.println("Inside UploadDataS3.uploadObject()");
		File outFile = new File("bannerg004_enc.mp4");
		try {
			FileEncryptionManager.encrypt(inFile,new FileOutputStream(outFile));
			PutObjectRequest objectRequest = new PutObjectRequest(bucketName, "Video/bannerg004_enc.mp4",outFile);
			s3client.putObject(objectRequest);
			if(outFile.exists()) {
				System.out.println("deleting the temp encrypted file==>"+outFile.getPath()+","+outFile.getAbsolutePath());
				outFile.delete();
			}
		} catch (Exception e) {
			System.out.println("Exception in uploadObject() ==>"+e.getMessage());
		}
	}
	
	private static File downloadObject(AmazonS3 s3client,String bucketName) throws Exception{
		System.out.println("Inside UploadDataS3.downloadObject()");
		InputStream in = s3client.getObject(bucketName, "Video/bannerg004_enc.mp4").getObjectContent();
		File outPutFile = new File("bannerg004_dec.mp4"); 	//locally stored file, planned to delete after session terminate.
		//File outPutFile = new File("C:\\Users\\Administrator\\Desktop\\personal\\s3_downloaded\\bannerg004_dec.mp4");
		FileEncryptionManager.decrypt(in,new FileOutputStream(outPutFile));
		System.out.println("download complete");
		return outPutFile;
	}
	
	private static AmazonS3 getS3Client(AWSCredentials awsCredentials) {
		return AmazonS3Client.builder()
	            .withCredentials((new AWSStaticCredentialsProvider(awsCredentials)))
	            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("s3.us-west-2.amazonaws.com",Regions.DEFAULT_REGION.getName()))
	            .withPathStyleAccessEnabled(true)
	            .build();
	}
}