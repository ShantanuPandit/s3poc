package com.poc.s3.insights.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.amazonaws.auth.AWSCredentials;
import com.poc.s3.insights.service.UploadDataS3;

@RestController
@RequestMapping("/s3")
public class BucketRestController {
	
	@Autowired
	AWSCredentials awsCredentials;

	@GetMapping("/upload")
	public String uploadFile(HttpServletRequest request,HttpServletResponse response) {
		InputStream inFile = null;
		File outPutFile = new File("sample_video.mp4"); 
		try {
			inFile = new FileInputStream(downloadFilefromURL("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4",outPutFile));
			//inFile = new FileInputStream(new File("C:\\Users\\Administrator\\Desktop\\personal\\bannerg004.mp4"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if(outPutFile.exists()) {
			System.out.println("deleting temp downloaded file ==>"+outPutFile.getPath()+","+outPutFile.getAbsolutePath());
			outPutFile.delete();
		}
		UploadDataS3.uploadFile(awsCredentials,"shantanu-9851-bucket",inFile);
		return "SUCCESS";
	}
	
	@GetMapping("/download")
	public void downloadFile(HttpServletRequest request,HttpServletResponse response) {
		File downloadedFile = UploadDataS3.downloadFile(awsCredentials,"shantanu-9851-bucket");
		
		if (null!=downloadedFile && downloadedFile.exists()) 
		{
			response.setContentType("video/mp4");
			response.addHeader("Content-Disposition", "attachment; filename="+downloadedFile.getName());
			try 
			{
				System.out.println("copying the file to response stream");
				Files.copy(downloadedFile.toPath(), response.getOutputStream());
				response.getOutputStream().flush();
				if(null!=downloadedFile && downloadedFile.exists()) {
					System.out.println("deleting the temp downloaded file from S3 ==>"+downloadedFile.getPath()+","+downloadedFile.getAbsolutePath());
					downloadedFile.delete();
				}
			} 
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static File downloadFilefromURL(String url,File outPutFile) throws Exception {
		
		try {
	        URLConnection urlConnection = new URL(url).openConnection();
	        urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36");
	        urlConnection.setReadTimeout(2000);
	        urlConnection.setConnectTimeout(2000);
	        
//	        InputStream in = urlConnection.getInputStream();
//			ReadableByteChannel rbc = Channels.newChannel(in);
//			FileOutputStream fos = new FileOutputStream(outPutFile);
//			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
//			return outPutFile;
			
			BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
			  FileOutputStream fileOutputStream = new FileOutputStream(outPutFile);

			    byte dataBuffer[] = new byte[1024];
			    int bytesRead;
			    while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
			        fileOutputStream.write(dataBuffer, 0, bytesRead);
			    }
			    return outPutFile;
			
		} catch (Exception e) {
			System.out.println("exception ==>" +e.getMessage());
		}
		return null;
	}
}