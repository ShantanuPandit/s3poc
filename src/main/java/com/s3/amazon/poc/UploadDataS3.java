package com.s3.amazon.poc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3EncryptionClientBuilder;
import com.amazonaws.services.s3.model.CryptoConfiguration;
import com.amazonaws.services.s3.model.CryptoMode;
import com.amazonaws.services.s3.model.EncryptionMaterials;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.StaticEncryptionMaterialsProvider;

public class UploadDataS3 {
	
	public  static String uploadObject(AWSCredentials awsCredentials,String bucketName,File inFile, File outFile) {
		try {
			AmazonS3 s3client = getS3Client(awsCredentials);
//			System.out.println("connection, success => "+s3client.getS3AccountOwner());	//checking retrieved account information
			//AmazonS3 s3clientEncrypt = getS3ClientEncrypt(awsCredentials);	//creating encryptedS3 connection for AWS server side encryption
			
/*			// creating bucket, if not present		
 * 			String bucketName = "shantanu-9851-bucket";
			if(s3client.doesBucketExist(bucketName)) {	//checking if bucket exists
			   System.out.println("bucket name already exist");
			   return "FAILURE";
			}			 
			s3client.createBucket(bucketName);	//creating bucket with custom name
			*/
			
			//listBuckets(s3client);	// listing S3 buckets
			listS3Objects(s3client, bucketName);	//listing S3 objects
			FileEncryptionManager.encrypt(new FileInputStream(inFile),new FileOutputStream(outFile));	//using client side encryption
			uploadObject(s3client, bucketName,outFile);	//uploading encrypted object
			downloadObject(s3client, bucketName);	//downloading and decryption
			inFile.delete();	//deleting temp files
			outFile.delete();	//deleting temp files
			
			//s3clientEncrypt.putObject(bucketName,"Document/encrypt_1",new File("C:\\Users\\Administrator\\Desktop\\personal\\sample.txt"));
			//System.out.println(s3clientEncrypt.getObjectAsString(bucketName, "Document/encrypt_1"));
			
		}catch(Exception e) {
			return e.getMessage();
		}
		return "SUCCESS";
	}
	
	private static void listS3Objects(AmazonS3 s3client, String bucketName) {
		s3client.listObjects(bucketName).getObjectSummaries().forEach(os->{
			System.out.println(os.getKey());
			System.out.println(os.getOwner());
		});
	}

	private static void uploadObject(AmazonS3 s3client,String bucketName,File file) {
		System.out.println("Inside UploadDataS3.uploadObject()");
		//PutObjectRequest objectRequest = new PutObjectRequest(bucketName,"Video/BirdNoSound.mp4",new File("C:\\Users\\Administrator\\Desktop\\personal\\BirdNoSound.mp4"));
		PutObjectRequest objectRequest = new PutObjectRequest(bucketName, "Video/bannerg004_enc.mp4",file);
		s3client.putObject(objectRequest);
		
	}

	private static void downloadObject(AmazonS3 s3client,String bucketName) throws IOException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException {
		System.out.println("Inside UploadDataS3.downloadObject()");
		InputStream in = s3client.getObject(bucketName, "Video/bannerg004_enc.mp4").getObjectContent();
		File outPutFile = new File("C:\\Users\\Administrator\\Desktop\\personal\\s3_downloaded\\bannerg004_dec.mp4");
		FileEncryptionManager.decrypt(in, new FileOutputStream(outPutFile));
		//Files.copy(in, Paths.get("C:\\Users\\Administrator\\Desktop\\personal\\s3_downloaded\\bannerg004.mp4"));
		
	}
	
	private static void listBuckets(AmazonS3 s3client) {
		s3client.listBuckets().forEach(bucket -> System.out.println(bucket.getName()));
	}

	private static AmazonS3 getS3Client(AWSCredentials awsCredentials) {

/*		// creating S3 client with specific region
		return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				  .withRegion(Regions.US_WEST_2)
				  .build();
		*/

		// creating S3 client with PARAM_URL and default region
		return AmazonS3Client.builder()
	            .withCredentials((new AWSStaticCredentialsProvider(awsCredentials)))
	            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("s3.us-west-2.amazonaws.com",Regions.DEFAULT_REGION.getName()))
	            .withPathStyleAccessEnabled(true)
	            .build();
	}
	
	// creating S3 client for AWS server side encryption
	private static AmazonS3 getS3ClientEncrypt(AWSCredentials awsCredentials) throws NoSuchAlgorithmException {
		
		 KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	     keyGenerator.init(256);

	     SecretKey secretKey = keyGenerator.generateKey();
	     
	return  AmazonS3EncryptionClientBuilder.standard()
	         .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
	         .withRegion(Regions.DEFAULT_REGION)
	         .withCryptoConfiguration(new CryptoConfiguration().withCryptoMode((CryptoMode.AuthenticatedEncryption)))
	         .withEncryptionMaterials(new StaticEncryptionMaterialsProvider(new EncryptionMaterials(secretKey)))
	         .build();
	}
}
