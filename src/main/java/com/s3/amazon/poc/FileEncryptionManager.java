package com.s3.amazon.poc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class FileEncryptionManager {
	
	private static final String secretKey = "abcd1234";
	private static final String salt = "defg5678";
	private static final int DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE = 1024;
	private static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public static OutputStream encrypt(InputStream in, OutputStream out) throws InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
		try {
			System.out.println("FileEncryptionManager.encryt().");
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	    	IvParameterSpec ivspec = new IvParameterSpec(iv);
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	        KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
	        SecretKey tmp = factory.generateSecret(spec);
	        SecretKeySpec secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");
	         
	        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
        	
            out = new CipherOutputStream(out, cipher);
            int count = 0;
            byte[] buffer = new byte[DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE];
            while ((count = in.read(buffer)) >= 0) {
                out.write(buffer, 0, count);
            }
        } finally {
            out.close();
        }
		System.out.println("encryption complete.");
        return out;
    }
	
	public static OutputStream decrypt(InputStream in, OutputStream out) throws InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IOException { 
		
		try {
			System.out.println("FileEncryptionManager.decryt().");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	    	IvParameterSpec ivspec = new IvParameterSpec(iv);
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	        KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
	        SecretKey tmp = factory.generateSecret(spec);
	        SecretKeySpec secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");
	        
	        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivspec);
	        out = new CipherOutputStream(out, cipher);
	        int count = 0;
	        byte[] buffer = new byte[DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE];
		        while ((count = in.read(buffer)) >= 0) {
		            out.write(buffer, 0, count);
		        }
		    } finally {
		        out.close();
		    }
		System.out.println("decryption complete.");
		return out;
	}
	

}
